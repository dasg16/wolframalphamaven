package com.wolframalpha.www;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Resources.Base;
import pageObjects.HomePage;
import pageObjects.Notebook;



//Test Cases has been prioritized based on the likelihood and impact of the defect in the requirement. 
public class TestWolframalpha extends Base {
	//Logging using Log4j
	public static Logger log = LogManager.getLogger(Base.class.getName());
	
	
	//Part.1.1. Initializes the driver and goes to the URL="http://www.wolframalpha.com/" in the browser.
	@BeforeTest
	public void initialize() throws IOException {
		driver =initializeDriver();
		driver.get(prop.getProperty("url"));
		log.info("Driver is initialized and url is invoked");
		
		//implicit wait
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		
		//Validate page URL.
		Assert.assertEquals(driver.getCurrentUrl(), "http://www.wolframalpha.com/");
		log.info("Validated page URL");
		
		//Validate page title.
		Assert.assertEquals(driver.getTitle(), "Wolfram|Alpha: Computational Intelligence");
		log.info("Validated page title");
	}
	
	//Part.1.2. Compute "2+2". After typing 2+2 in the search field, hit Enter or click the equal sign icon to get results.
	@Test(priority = 1)
	public void compute() throws IOException, InterruptedException {
		
		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream("/Users/gouravdas/Wolframalpha/src/main/java/Resources/Untitled");
	
		prop.load(fis);
		
		HomePage homep = new HomePage(driver);
		
		
		//Using action class to move to the search bar, insert values in it and evaluate it.
		Actions a = new Actions(driver);

		a.moveToElement(homep.getSearchBar()).click().sendKeys("2+2").build().perform();
		a.moveToElement(homep.getEqualButton()).click().build().perform();
		log.info("Entered 2+2 in the search bar and clicked the equal button");
		
		String input= homep.getInputString().getAttribute("alt");
		input= input.replaceAll(" ", "");
		
		//Validate if the input string in the input pod is correct.
		Assert.assertEquals(input, "2+2");
		log.info("Validated if the input string in the input pod is correct");		
		
		String result= homep.getResultString().getAttribute("alt");

		//Validate if the calculation of result pod is correct.
		Assert.assertEquals(result, "4");
		log.info("Validated if the calculation of result pod is correct");		
		
		//Using explicit wait for the notebook page to load completely.
		WebDriverWait d= new WebDriverWait(driver,20);
		d.until(ExpectedConditions.visibilityOfElementLocated(homep.getOpenCodeVisible()));
				
		
		//Validating if the input pod and open code button are present after computing.
		Assert.assertTrue(homep.getInputPod().isDisplayed());
		Assert.assertTrue(homep.getOpenCode().isDisplayed());
		log.info("Validated if the input pod and open code button are present after computing");		
	}
	
	//Part.1.3. Hover over the input pod and click on the "Open Code" feature.
	@Test(priority = 2)
	public void validateOpenCode() {
		
		HomePage homep = new HomePage(driver);
		Actions a = new Actions(driver);
		//Hovering exactly over the input pod cell where Open Code button becomes enabled.  
		a.moveToElement(homep.getInputPod()).build().perform();
		
		//Validate if the code button in enabled after pointing at the cell where Open Code button is mentioned.
		Assert.assertTrue(homep.getOpenCode().isEnabled());
		log.info("Validated if the code button in enabled after pointing at the cell where Open Code button is mentioned");		
		
		a.moveToElement(homep.getOpenCode()).click().build().perform();
		
		
		
	}
	
	

	//Part.1.4. Find a cell containing "2+2",click the orange play button, 2+2 will evaluate and return 4.
	@Test(priority = 3)
	public void notebookPlayBtn() throws InterruptedException {
		
		Notebook noteB = new Notebook(driver);
		
		String addnInput =noteB.getInputCell().getText();
		String alphabets =  addnInput.replaceAll("\n", " ");
		//Validating the text 2+2 in the input cell.
		Assert.assertEquals(alphabets, "2 + 2");
		log.info("Validated the text 2+2 in the input cell");
		
		//Using explicit wait for the notebook page to load completely.
		WebDriverWait d= new WebDriverWait(driver,40);
		d.until(ExpectedConditions.visibilityOfElementLocated(noteB.getPlayButtonVisible()));
		
		
		noteB.getPlayButton().click();
		String addnOutput = noteB.getAdditionOutput().getText();
		log.info("Clicked on the orange play button");
		
		//validating if the output of the addition is as expected.
		Assert.assertEquals(addnOutput, "4");
		log.info("Validated if the output of the addition is as expected");
		

		
	//Part.1.5. Click the oval-shaped button "What's This?" and open a page explaining Open Code feature
		driver.switchTo().frame(noteB.getWhatIsThisFrame());
		
		String validWhatsName = noteB.getWhatIsThisTab().getText();
		
		
		//Validate if anything with "What's This?" has been displayed on the screen.
		Assert.assertEquals(validWhatsName, "What's This?");
		log.info("Validated What's This? text has been displayed on the screen");
		
		noteB.getWhatIsThisTab().click();
	
	}
	
	@Test(priority = 4)
	public void validateWhatIsThis() {
		
		//Validate the current page name before moving to whats this page.
		Assert.assertEquals(driver.getTitle(), "(unnamed) - Wolfram Cloud");
		log.info("Validated the current page name before moving to whats this page");
		
		Set<String>ids=driver.getWindowHandles();
		Iterator<String> it= ids.iterator();
		String parentID = it.next();
		String childID = it.next();
		
		
		driver.switchTo().window(childID);
		
		// Validating the child page name by switching into whats this page from the parent page.
		Assert.assertEquals(driver.getTitle(), "Explore the Wolfram Language Code behind Wolfram|Alpha");
		log.info("Validated the child page name by switching into whats this page from the parent page");
		
		driver.switchTo().window(childID).close();;
		driver.switchTo().window(parentID);
		
		
		//Validate the current page name after moving to whats this page.
		Assert.assertEquals(driver.getTitle(), "(unnamed) - Wolfram Cloud");
		log.info("Validated the current page name after moving to whats this page");
	
	}
	
	//Step.1.6. Find a side menu in the notebook with a list of links to variety of educational resources.
	@Test(priority = 5)
	public void notebookEduResources() {
		
		Notebook noteB = new Notebook(driver);
		
		//taking the control to the side menu which is the educational resources section.
		//In this Step 1.6, there are four equivalent classes in the side menu of educational resources. 
		
		// The first equivalent class for the image.
		//WebElement element = driver.FindElement(By.XPath("Your xpath"));
		WebElement element = noteB.getImageSource();
		String path = element.getAttribute("src");
		
		//Validate the source link of the image
		Assert.assertEquals(path, "https://sandbox.open.wolframcloud.com/app/images/wolframAlphaSidebar/mathHeader.svg");
		log.info("Validated the source link for the image in Educational Resources section");
		
	}
	
	
		@Test(priority = 6)
		public void computeAnythingSection() {
			
		Notebook noteB = new Notebook(driver);
		
		// The second equivalent class for the Compute Anything section.
		WebElement computeAnything = noteB.getComAnythingSection();
		
		int countCA = computeAnything.findElements(By.tagName("a")).size();
		for(int i=0; i<countCA; i++) {
			String pageLink = computeAnything.findElements(By.tagName("a")).get(i).getAttribute("href");
			
			
			WebDriverWait d= new WebDriverWait(driver,40);
			d.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("a")));
			
			List<WebElement> element1 = computeAnything.findElements(By.tagName("a"));
			Actions actions = new Actions(driver);
			actions.moveToElement(element1.get(i)).click().build().perform();
			
			
			Set<String>ids=driver.getWindowHandles();
			Iterator<String> it= ids.iterator();
			String parentID = it.next();
			String childID = it.next();
			
			
			driver.switchTo().window(childID);
			String currentPageURL = driver.getCurrentUrl();
			
			driver.close();;
			driver.switchTo().window(parentID);
			
			//Validate if the links in the Compute Anything section actually opens the same link and not redirect to any other links.
			Assert.assertTrue(currentPageURL.toLowerCase().contains(pageLink.toLowerCase()));
			log.info("Validated if the links in the Compute Anything section actually opens the same link and not redirect to any other links");
		}
		}
		
		
		@Test(priority = 7)
		public void learnToCodeSection() {
		Notebook noteB = new Notebook(driver);
		// The third equivalent class for the Learn To Code section.
		WebElement learnToCode = noteB.getLrnToCodeSection();
		int countLTC = learnToCode.findElements(By.tagName("a")).size();

		for(int i=0; i<countLTC; i++) {
			
			
			String pageLink = learnToCode.findElements(By.tagName("a")).get(i).getAttribute("href");			
			
			//Using JavaScriptExecutor because otherwise the links are not clickable. Using explicit wait ExpectedConditions.elementToBeClickable worked for a while.
			WebElement element = learnToCode.findElements(By.tagName("a")).get(i);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			

		
			Set<String>ids2=driver.getWindowHandles();
			Iterator<String> it2= ids2.iterator();
			String parentID = it2.next();
			
			String childID = it2.next();
			
			
			driver.switchTo().window(childID);
			String currentPageURL = driver.getCurrentUrl();
			driver.close();
			driver.switchTo().window(parentID);
			if(currentPageURL.contains("http:")) {
				
				 pageLink = pageLink.replaceAll("https:", "http:");
				
			}
			
			//Validate if the links in the Learn To Code section actually opens the same link and not redirect to any other links.
			Assert.assertTrue(currentPageURL.toLowerCase().contains(pageLink.toLowerCase()));
			log.info("Validated if the links in the Learn To Code section actually opens the same link and not redirect to any other links");
		}
		
		
		}
		
		@Test(priority = 8)
		public void validateFooter() {
		
		Notebook noteB = new Notebook(driver);
			
		// The forth equivalent class for the footer has two more sub equivalent classes: Create New Notebook and Documentation.
		// Validating Create New Notebook.
		noteB.getCreateNewNote().click();
		Set<String>ids=driver.getWindowHandles();
		Iterator<String> it= ids.iterator();
		String parentID = it.next();
		String childID = it.next();
		
		
		driver.switchTo().window(childID);
		
		// Validating the child page name by switching into it from the parent page.
		Assert.assertEquals(driver.getTitle(), "Wolfram Language Sandbox(Open Cloud)");
		log.info("Validated the child page name by clicking into Create New Note button switching into it from the Educational Resources footer");
		
		driver.close();;
		driver.switchTo().window(parentID);
		
		
		//Validate the parent page again after switching back from the child page.
		Assert.assertEquals(driver.getTitle(), "(unnamed) - Wolfram Cloud");
		log.info("Validated the parent page Educational Resources footer by returning from Create New Note child page");
		
		noteB.getDocumentation().click();
		driver.switchTo().frame("docCenter");
		
		//Validate if clicking on the Documentation in the footer opens up the iframe: "WOLFRAM LANGUAGE & SYSTEM" with the id ="docCenter".
		String DocCenterTitle= noteB.getDocCenterTitle().getText();
		Assert.assertEquals(DocCenterTitle, "WOLFRAM LANGUAGE & SYSTEM");
		log.info("Validated if clicking on the Documentation in the footer opens up the iframe: \"WOLFRAM LANGUAGE & SYSTEM\" with the id =\"docCenter\"");
		
		//Need to switch back to default content because close button for the frame is outside the frame.
		driver.switchTo().defaultContent();
		noteB.getCloseButton().click();
	
	
	}
	
	
	@AfterTest
	public void teardown(){
		driver.quit();
		driver = null;
		log.info("Quit driver and set it to null");
	}
	

}

