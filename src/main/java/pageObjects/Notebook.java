package pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Notebook {
	
public WebDriver driver;
	
	By inputCell=By.xpath("//*[@id=\'notebookContent\']/div/div[3]/div/div[8]/div/div[2]/div/div/div/div[4]/div/div[1]/div/div/div/div[3]/div/pre/div/div/div/div");
	By playButton=By.xpath("//*[@id='notebookContent']/div/div[3]/div/div[7]/div/div[2]/div/div/div/div[4]/div/div[1]/div/div/div/div[3]/div/pre/div/div/div/div/div/div/div/div/div/div/table/tbody/tr[1]/td[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div");
	By additionOutput=By.xpath("//*[@id=\'notebookContent\']/div/div[3]/div/div[9]/div/div[2]/div/div/div/span/span");
	By whatsThisFrame=By.xpath("//*[@id=\"notebookContent\"]/div/div[3]/div/div[3]/div/div[2]/div/div/div/iframe");
	By whatsThis=By.cssSelector("#whatsthis");
	By eduResources=By.xpath("//*[@id=\'mathematicaOnlineFileViewLayout\']/div[2]/div[2]");
	By computeAnything=By.xpath("//*[@id=\'mathSidebar\']");
	By learnToCode=By.xpath("//*[@id=\'nonmathSidebar\']");
	By comAnyLinks=By.tagName("a");
	By imageSource=By.xpath("//*[@id=\'wolframAlphaSidebar\']/img");
	By createNewNote=By.xpath("//*[@id=\'waNewNb\']");
	By Documentation=By.xpath("//*[@id=\'waDocCenter\']");
	By docCenterTitle= By.xpath("//*[@id=\'languageRootGuide\']/main/header/div[1]/a[1]/span[1]");
	By closeButton= By.xpath("//*[@id=\'palette-close\']");
	
	
	
	public Notebook(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
	}
	
	
	public WebElement getInputCell() {
		
		return driver.findElement(inputCell);
		
		
	}



	public WebElement getPlayButton() {
		
		return driver.findElement(playButton);
		
		
	}
	
	public By getPlayButtonVisible() {
		
		return playButton;
		
		
	}
	
	public WebElement getAdditionOutput() {
		
		return driver.findElement(additionOutput);
		
		
	}
	
	public WebElement getWhatIsThisFrame() {
		
		return driver.findElement(whatsThisFrame);
		
		
	}
	
	public WebElement getWhatIsThisTab() {
		
		return driver.findElement(whatsThis);
		
		
	}
	
	public WebElement getEduResSection() {
		
		return driver.findElement(eduResources);
		
		
	}
	
	
	public WebElement getComAnythingSection() {
		
		return driver.findElement(computeAnything);
		
		
	}
	
	public WebElement getLrnToCodeSection() {
		
		return driver.findElement(learnToCode);
		
		
	}
	
	public By getLrnToCodeVisible() {
		
		return learnToCode;
		
		
	}
	
	public List<WebElement> getComAnySecLinks() {
		
		return driver.findElements(comAnyLinks);
		
		
	}
	
	public WebElement getImageSource() {
		
		return driver.findElement(imageSource);
		
		
	}
	
	public WebElement getCreateNewNote() {
		
		return driver.findElement(createNewNote);
		
		
	}

	public WebElement getDocumentation() {
	
		return driver.findElement(Documentation);
	
	
	}
	
	public WebElement getDocCenterTitle() {
		
		return driver.findElement(docCenterTitle);
	
	
	}
	
	public WebElement getCloseButton() {
		
		return driver.findElement(closeButton);
	
	
	}
	

}

