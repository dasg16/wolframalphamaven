package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {
	
	public WebDriver driver;
	
	
	By searchBox=By.xpath("//*[@id=\'query\']");
	By equalButton=By.xpath("//*[@id=\'input\']/fieldset/div/input[2]");
	By inputString=By.xpath("//*[@id=\'Input\']/section/div[1]/div/img");
	By resultString=By.xpath("//*[@id=\'Result\']/section/div[1]/div/img");
	By inputPod=By.xpath("//*[@id=\'Input\']/section/div[2]");
	By openCode=By.xpath("//*[@id=\'Input\']/section/div[2]/div/p");
	
	
	public HomePage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
	}



	public WebElement getSearchBar() {
		
		return driver.findElement(searchBox);
		
		
	}
	
	public WebElement getEqualButton() {
		
		return driver.findElement(equalButton);
		
		
	}
	
	
	public WebElement getInputString() {
		
		return driver.findElement(inputString);
		
		
	}
	
	public WebElement getResultString() {
		
		return driver.findElement(resultString);
		
		
	}
	
	
	public WebElement getInputPod() {
		
		return driver.findElement(inputPod);
		
		
	}
	
	
	public WebElement getOpenCode() {
		
		return driver.findElement(openCode);
		
		
	}
	
	public By getOpenCodeVisible() {
		
		return openCode;
		
		
	}

}

